/* @flow */

import { artistsService } from '../service/artists.service';

export const FETCH_ARTISTS = 'FETCH_ARTISTS';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_ERROR = 'FETCH_ARTISTS_ERROR';

export const ARTIST_SELECTED = 'ARTIST_SELECTED';

export const FETCH_ARTIST_SONGS = 'FETCH_ARTIST_SONGS';
export const FETCH_ARTIST_SONGS_ABORT = 'FETCH_ARTIST_SONGS_ABORT';
export const FETCH_ARTIST_SONGS_SUCCESS = 'FETCH_ARTIST_SONGS_SUCCESS';
export const FETCH_ARTIST_SONGS_ERROR = 'FETCH_ARTIST_SONGS_ERROR';

export const createFetchArtistsSongsAbort = () => {
	return {
		type: FETCH_ARTIST_SONGS_ABORT
	}
};

export const createFetchArtistsAction = () => {
	return {
		type: FETCH_ARTISTS
	}
};

export const createFetchArtistSongsAction = (artistId: string) => {
	return {
		type: FETCH_ARTIST_SONGS,
		payload: {
			artistId: artistId
		}
	}
};

export const createArtistSelectedAction = (artistId: string) => {
	return {
		type: ARTIST_SELECTED,
		payload: {
			artistId: artistId
		}
	};
};

export const createFetchArtistsSuccessAction = (artists: Object) => {
	return {
		type: FETCH_ARTISTS_SUCCESS,
		payload: {
			artists: artists
		}
	};
};

export const createFetchArtistsErrorAction = (error: Object) => {
	console.log(error);

	return {
		type: FETCH_ARTISTS_ERROR,
		error: error
	};
};

export const fetchArtists = () => {
	return (dispatch: Function) => {
		artistsService
			.fetchArtists()
			.then((data) => dispatch(createFetchArtistsSuccessAction(data)))
			.catch((error) => dispatch(createFetchArtistsErrorAction(error)));
	};
};

export const createFetchArtistSongsSuccessAction = (songs: Object) => {
	return {
		type: FETCH_ARTIST_SONGS_SUCCESS,
		payload: {
			songs: songs
		}
	};
};

export const createFetchArtistSongsErrorAction = (error: Object) => {
	console.log(error);

	return {
		type: FETCH_ARTIST_SONGS_ERROR,
		error: error
	};
};

export const fetchArtistSongs = (artistId: string) => {
	return (dispatch: Function) => {
		artistsService
			.fetchArtistSongs(artistId)
			.then((data) => dispatch(createFetchArtistSongsSuccessAction(data)))
			.catch((error) => dispatch(createFetchArtistSongsErrorAction(error)));
	};
};
