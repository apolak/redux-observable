/* @flow */

import * as React from 'react';

import type { Artist } from '../../domain/types';

type ArtistSelectPageProps = {
	artists: Artist[],
	onArtistSelect: (artistId: string) => void,
	fetchArtists: () => void
}

export class ArtistSelectPage extends React.Component {
	props: ArtistSelectPageProps;

	componentWillMount() {
		if (!this.hasArtists()) {
			this.props.fetchArtists();
		}
	}

	hasArtists() {
		return this.props.artists.length > 0;
	}

	handleChange(event: Object) {
		this.props.onArtistSelect(event.target.value);
	}

	render() {
		if (this.hasArtists()) {
			return (
				<div>
					<select value="" onChange={this.handleChange.bind(this)}>
						<option value="" disabled>Please Choose</option>
						{
							this.props.artists.map((artist) => {
								return (<option key={artist.id} value={artist.id}>{artist.name}</option>);
							})
						}
					</select>
				</div>
			);
		}

		return (
			<div>
				<div className="loader">Imma chargin ma artists!!!!</div>
			</div>
		);
	}
}