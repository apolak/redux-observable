/* @flow */

import { connect } from 'react-redux';
import { ArtistSelectPage } from './artist-select.page.jsx';
import { fetchArtists, createArtistSelectedAction, createFetchArtistsAction } from '../../action/artists.action';
import { push } from 'react-router-redux';

const mapStateToProps = (state) => {
	return {
		artists: state.artists
	};
};

const mapDispatchToProps = (dispatch: Function) => {
	return {
		onArtistSelect: (artistId: string) => {
			dispatch(createArtistSelectedAction(artistId));
			dispatch(push('/artist/' + artistId));
		},
		fetchArtists: () => {
			//dispatch(createFetchArtistsAction());
			dispatch(fetchArtists());
		}
	};
};

export const ConnectedArtistSelectPage = connect(mapStateToProps, mapDispatchToProps)(ArtistSelectPage);
