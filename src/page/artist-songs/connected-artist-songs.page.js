/* @flow */

import { connect } from 'react-redux';
import { ArtistSongsPage } from './artist-songs.page.jsx';
import { fetchArtistSongs, createFetchArtistsSongsAbort, createFetchArtistSongsAction } from '../../action/artists.action';

const mapStateToProps = (state) => {
	return {
		songs: state.activeArtistSongs
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		onBack: () => {
			dispatch(createFetchArtistsSongsAbort());
		},
		fetchArtistSongs: (artistId: string) => {
			//dispatch(createFetchArtistSongsAction(artistId));
			dispatch(fetchArtistSongs(artistId));
		}
	};
};

export const ConnectedArtistSongsPage = connect(mapStateToProps, mapDispatchToProps)(ArtistSongsPage);
