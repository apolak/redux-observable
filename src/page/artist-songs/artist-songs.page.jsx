/* @flow */

import * as React from 'react';

import type { Song } from '../../domain/types';
import { Link } from 'react-router'

type ArtistSongsPageProps = {
	onBack: () => void,
	songs: Song[],
	fetchArtistSongs: (artistId: string) => void,
	params: Object
}

export class ArtistSongsPage extends React.Component {
	props: ArtistSongsPageProps;

	componentWillMount() {
		if (!this.hasSongs()) {
			this.props.fetchArtistSongs(this.props.params.id);
		}
	}

	hasSongs() {
		return this.props.songs.length > 0;
	}

	render() {
		if (this.hasSongs()) {
			return (
				<div>
					<Link to='/' onClick={this.props.onBack}>Show artists</Link>
					{
						this.props.songs.map((song) => {
							return (
								<p key={song.id}>{song.title}</p>
							);
						})
					}
				</div>
			);
		}

		return (
			<div>
				<Link to='/' onClick={this.props.onBack}>Show artists</Link>
				<div className="loader">Imma chargin ma songs!!!!</div>
			</div>
		);
	}
}