/* @flow */

export const artistsService: Object = {
	fetchArtists: () => {
		return fetch('http://localhost:3333/api/artists')
			.then((response) => response.json())
	},
	fetchArtistSongs: (artistId) => {
		return fetch('http://localhost:3333/api/artists/' + artistId + '/songs')
			.then((response) => response.json());
	}
};
