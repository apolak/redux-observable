import AppComponent from '../components/app.component.jsx';
import { ConnectedArtistSelectPage } from '../page/artist-select/connected-artist-select.page.js';
import { ConnectedArtistSongsPage } from '../page/artist-songs/connected-artist-songs.page.js';

export const routes = {
	path: '/',
	component: AppComponent,
	indexRoute: { component: ConnectedArtistSelectPage },
	childRoutes: [
		{
			path: 'artist/:id',
			component: ConnectedArtistSongsPage
		}
	]
};
