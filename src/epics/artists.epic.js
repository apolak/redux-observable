/* @flow */

import { combineEpics } from 'redux-observable';
import { ajax } from 'rxjs/observable/dom/ajax';

import {
	FETCH_ARTISTS,
	FETCH_ARTISTS_SUCCESS,
	FETCH_ARTISTS_ERROR,
	FETCH_ARTIST_SONGS,
	FETCH_ARTIST_SONGS_SUCCESS,
	FETCH_ARTIST_SONGS_ERROR,
	FETCH_ARTIST_SONGS_ABORT
} from '../action/artists.action';

const fetchArtistsEpic = action$ =>
	action$.ofType(FETCH_ARTISTS)
		.debounceTime(200)
		.switchMap(action =>
			ajax.getJSON('http://localhost:3333/api/artists')
				.map(payload => ({
					type: FETCH_ARTISTS_SUCCESS,
					payload: {
						artists: payload
					}
				}))
				.catch(payload => ({
					type: FETCH_ARTISTS_ERROR,
					error: payload
				}))
		);

const fetchArtistSongsEpic = action$ =>
	action$.ofType(FETCH_ARTIST_SONGS)
		.debounceTime(200)
		.switchMap(action =>
			ajax.getJSON('http://localhost:3333/api/artists/' + action.payload.artistId + '/songs')
				.map(payload => ({
					type: FETCH_ARTIST_SONGS_SUCCESS,
					payload: {
						songs: payload
					}
				}))
				.takeUntil(action$.ofType(FETCH_ARTIST_SONGS_ABORT))
				.catch(payload => ({
					type: FETCH_ARTIST_SONGS_ERROR,
					error: payload
				}))
		);

export const rootEpic = combineEpics(
	fetchArtistsEpic,
	fetchArtistSongsEpic
);
