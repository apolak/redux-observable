/* @flow */

import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { songsReducer } from './songs.reducer';
import { artistsReducer } from './artists.reducer';

export const reducers = combineReducers({
	activeArtistSongs: songsReducer,
	artists: artistsReducer,
	routing: routerReducer
});
