/* @flow */

import { FETCH_ARTIST_SONGS_SUCCESS, ARTIST_SELECTED } from '../action/artists.action';

export const songsReducer = (state: Array<Object> = [], action: Object) => {
	switch (action.type) {
		case ARTIST_SELECTED:
			return [];
		case FETCH_ARTIST_SONGS_SUCCESS:
			return action.payload.songs;
		default:
			return state;
	}
};
