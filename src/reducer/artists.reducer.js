/* @flow */

import { FETCH_ARTISTS_SUCCESS } from '../action/artists.action';

export const artistsReducer = (state: Array<Object> = [], action: Object) => {
	switch (action.type) {
		case FETCH_ARTISTS_SUCCESS:
			return action.payload.artists;
		default:
			return state;
	}
};
