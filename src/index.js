/* @flow */

//import 'rxjs';
import React from 'react';
import ReactDom from 'react-dom';

import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
//import { createEpicMiddleware } from 'redux-observable';

//import { rootEpic } from './epics/artists.epic';

import { reducers } from './reducer/index';
import { routes } from './config/routes';

//const epicMiddleware = createEpicMiddleware(rootEpic);

const initialState = {
	artists: [],
	activeArtistSongs: []
};

const store = createStore(
	reducers,
	initialState,
	composeWithDevTools(
		applyMiddleware(thunk),
		//applyMiddleware(epicMiddleware),
		applyMiddleware(routerMiddleware(browserHistory))
	)
);

const history = syncHistoryWithStore(browserHistory, store);

ReactDom.render(
	<Provider store={store}>
		<Router history = {history} routes={routes}/>
	</Provider>,
	document.getElementById('root')
);
