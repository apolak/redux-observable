/* @flow */

import React from 'react';

export type Artist = {
	id: string,
	name: string
};

export type Song = {
	id: string,
	title: string,
	price: number
};