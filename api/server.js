'use strict';

const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const NodeCache = require('node-cache');

const app = express();

app.disable('etag');

app.set('db', new NodeCache());

app.use(logger('dev'));
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api', require('./routes/api.js'));

app.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

if (app.get('env') === 'development') {
	app.use((err, req, res, next) => {
		res
			.status(err.status || 500)
			.json({
				message: err.message,
				error: err
			});
	});
}

app.use((err, req, res, next) => {
	res
		.status(err.status || 500)
		.json({
			message: err.message,
			error: {}
		});
});

app.listen(3333);
console.log("server started on port 3333");
