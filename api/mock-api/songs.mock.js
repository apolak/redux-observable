module.exports = [
	{
		id: 'song-1',
		artistId: 'author-1',
		title: 'Kill \'em all',
		price: '0.99'
	},
	{
		id: 'song-2',
		artistId: 'author-2',
		title: 'Blood brothers',
		price: 0.99
	},
	{
		id: 'song-3',
		artistId: 'author-2',
		title: 'Fear of the dark',
		price: 0.99
	},
	{
		id: 'song-4',
		artistId: 'author-3',
		title: 'Ecstasy of gold',
		price: 0.99
	},
	{
		id: 'song-5',
		artistId: 'author-4',
		title: 'Protectors of the earth',
		price: 0.99
	},
	{
		id: 'song-6',
		artistId: 'author-4',
		title: 'Heart of courage',
		price: 0.99
	}
];
