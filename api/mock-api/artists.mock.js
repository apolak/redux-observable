module.exports = [
	{
		id: 'author-1',
		name: 'Metallica'
	},
	{
		id: 'author-2',
		name: 'Iron Maiden'
	},
	{
		id: 'author-3',
		name: 'Ennio Morricone'
	},
	{
		id: 'author-4',
		name: 'Two Steps From Hell'
	}
];
