var express = require('express');
var router = express.Router();

router.use('/artists', require('./v1/artists.js'));

module.exports = router;
