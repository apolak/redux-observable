var express = require('express');
var router = express.Router();

router.get('/', require('./artists/get-artists.js'));
router.get('/:id/songs', require('./artists/get-artist-songs.js'));

module.exports = router;
