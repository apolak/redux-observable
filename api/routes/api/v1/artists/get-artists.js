'use strict';

const artists = require('../../../../mock-api/artists.mock');

module.exports = (req, res) => {
	setTimeout(() => res.json(artists), 5000);
};
