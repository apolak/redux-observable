'use strict';

const songs = require('../../../../mock-api/songs.mock');

module.exports = (req, res) => {
	const artistId = req.params.id;

	const artistSongs = songs.filter((song) => song.artistId === artistId);

	if (artistSongs.length > 0) {
		setTimeout(() => res.json(artistSongs), Math.random() * 10000);
	} else {
		res.status(404).json({
			'status': 'not-found',
			'message': 'Could not find songs for artist'
		});
	}
};
