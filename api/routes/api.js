var express = require('express');
var router = express.Router();

router.use('/', require('./api/v1.js'));

module.exports = router;
